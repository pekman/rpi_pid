import RPi.GPIO as GPIO
import time
import grove_temperature_humidity_aht20
import sys
sensor = grove_temperature_humidity_aht20.GroveTemperatureHumidityAHT20()

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(18,GPIO.OUT)
pi_pwm = GPIO.PWM(18,1000)#create PWM instance with frequency
pi_pwm.start(0)#start PWM of required Duty Cycle 

time_list=[]
current_time=0
target_time=60*5

initial_temperature, initial_humidity  = sensor.read()
time.sleep(0.1)
initial_temperature, initial_humidity  = sensor.read()
print(f"Initial temperature is {initial_temperature} C\n")

target_temperature = 27.0

kp = 33
ki = 1

integral_list=[]
saturated = False

GPIO.output(18,GPIO.HIGH)

duty_upper_limit = 100
duty_lower_limit = 0

print("Time (s)\tTemperature (C)\tTemperature Diff (C)\tDuty")
try:
    while current_time<target_time:
        time_list.append(current_time)
        t0 = time.monotonic()
        
        current_temperature, current_humidity  = sensor.read()
        temperature_difference = target_temperature - current_temperature
        if saturated != True:
            integral_list.append(temperature_difference)
        
        P = temperature_difference * kp
        integral = sum(integral_list)
        I = integral * ki
        
        proposed_duty = P + I
        
        if proposed_duty > duty_upper_limit:
            saturated = True
            applied_duty = duty_upper_limit
        elif proposed_duty < duty_lower_limit:
            saturated = True
            applied_duty = duty_lower_limit
        else:
            saturated = False
            applied_duty = proposed_duty
        print(f"{round(current_time)}\t\t{round(current_temperature,1)}\t\t{round(temperature_difference,1)}\t\t\t{round(applied_duty,1)}\t{round(P)}\t{round(I)}")
        
        pi_pwm.ChangeDutyCycle(applied_duty)
        # Calculate the time taken for everything and add what is needed for it to take 1 second to run
        # Time stuff
        t1 = time.monotonic()
        dt = t1 - t0
        sleep_time = 1 - dt
        try:
            time.sleep(sleep_time)
            current_time = current_time + dt +sleep_time
        except ValueError:
            current_time = current_time + dt
            

except KeyboardInterrupt:
    pi_pwm.stop()
    sys.exit()
except:
    pi_pwm.stop()
    sys.exit()